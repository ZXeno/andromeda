﻿using System;
using System.IO;
using Andromeda.Infrastructure;

namespace Andromeda
{
    public class Program
    {
        public const string VersionNumber = "Version 0.4";

        public static string WorkingPath = Environment.CurrentDirectory;
        public static string UserFolder = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Andromeda";
        public static string DirectoryToCheckForUpdate = "\\\\melvin\\Andromeda\\";

        private Logger _logger;

        private static CredentialManager _credman;
        public static CredentialManager CredentialManager
        {
            get { return _credman; }
            set { _credman = value; }
        }

        public static ResultConsole ResultConsole { get; private set; }

        private static ConfigManager _configMan;
        public static ConfigManager ConfigManager { get { return _configMan; } }
        private static bool _updateAvailable;
        public static bool UpdateAvailable { get { return _updateAvailable; } }

        public Program()
        {
            if (!Directory.Exists(UserFolder))
            {
                Directory.CreateDirectory(UserFolder);
            }

            _logger = new Logger();
            _credman = new CredentialManager();
            _configMan = new ConfigManager();
            ResultConsole = ResultConsole.Instance;

            CheckForNewVersion();
        }

        private void CheckForNewVersion()
        {
            if(Directory.Exists(DirectoryToCheckForUpdate))
            {
                if (File.Exists(DirectoryToCheckForUpdate + "andromeda.exe"))
                {
                    var hostedAndromeda = File.GetLastWriteTimeUtc(DirectoryToCheckForUpdate + "andromeda.exe");
                    var localAndromeda = File.GetLastWriteTimeUtc(WorkingPath + "\\andromeda.exe");
                    var result = hostedAndromeda.CompareTo(localAndromeda);
                    if (result > 0)
                    {
                        _updateAvailable = true;
                    }
                }
            }
        }
    }
}