﻿namespace Andromeda
{
    public enum ActionGroup
    {
        SCCM,
        WindowsManagement,
        Maintenance,
        Other,
        Debug
    }
}